# Seafight #

Seafight game.

## Getting Started ##

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites ###
* node.js or docker

### Installing vie webpack ###
* Download project ( git clone https://bitbucket.org/haanastasia/seafight.git )
* Run node.js
* npm run build
* npm run start

### Installing vie docker ###
* Download project ( git clone git clone https://bitbucket.org/haanastasia/seafight.git )
* Run docker
* docker build -t seafight .
* docker run -p 30000:30000 seafight
* open http://localhost:30000

## Built With ##
* Webpack
* Docker
* SASS
* ES6

## Authors ##

* Anastasia Dolgopolova (https://bitbucket.org/haanastasia/seafight/)
import Seafight from './seafight/main';

export default class FrontController {
	
	init() {
		this.SeafightController();
	}
	
	SeafightController() {
		let game = new Seafight;
		game.init();
	}

}
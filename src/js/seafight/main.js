import FieldFabric from './core/field/FieldFabric';
import LocationShipsFabric from './core/locationShips/LocationShipsFabric';
import { checkLocationShip } from './helpers/checkLocationShip';
import Battle from './core/battle/Battle';


export default class Seafight {

	// генерируем поле для пользователя и для компьютера
	createField( fieldType ) {
		return FieldFabric.createFieldByType( fieldType );
	}

	// методы расстановки кораблей: вручную или рандомно
	createLocationShips( locationType ) {
		return LocationShipsFabric.createLocationByType( locationType );
	}
	
	// запускаем битву 
	createBattle() {
		new Battle();
	}	
}
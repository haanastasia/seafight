import BaseField from './BaseField';

class UserField extends baseField {

    constructor() {
        super(); // вызываем конструктор родителя
        this.classesList.push('field__cell--user'); // добавляем класс
    }

}

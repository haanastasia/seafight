import UserField from './UserField';
import PcField from './PcField';

class FieldFabric {

    constructor() {
        this.fieldTypes = {
            'user': UserField,
            'pc': PcField
        };
    }

    createFieldByType(fieldType) {
        if (this.fieldTypes.hasOwnProperty(fieldType)) {
            return new this.fieldTypes[fieldType];
        }
    }

}

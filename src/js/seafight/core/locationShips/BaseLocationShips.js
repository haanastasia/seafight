import BaseField from '../field/BaseField';

export default class BaseLocationShips {

    constructor() {
        // массив с данными кораблей
        this.shipsData = [
            [4, 'fourdeck'],
            [3, 'tripledeck'],
            [2, 'doubledeck'],
            [1, 'singledeck']
        ];
        // координаты
        this.matrix = BaseField.createMatrix();
    }

}
import ManuallyLocationShips from './ManuallyLocationShips';
import RandomLocationShips from './RandomLocationShips';

class LocationShipsFabric {

    constructor() {
        this.locationTypes = {
            'manually': ManuallyLocationShips,
            'random': RandomLocationShips
        };
    }

    createLocationByType(locationType) {
        if (this.locationTypes.hasOwnProperty(locationType)) {
            return new this.locationTypes[locationType];
        }
    }

}
